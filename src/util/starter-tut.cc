/*
 * Copyright (C) 2007  Enrico Zini <enrico@enricozini.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

#include "tests/test-utils.h"
#include "util/starter.h"

#include <sstream>
#include <iostream>

namespace tut {
using namespace std;
using namespace util;

struct starter_shar {
};
TESTGRP(starter);

struct TestStarter : public Startable
{
	static unsigned seq;
	bool started;
	unsigned startedWhen;
	unsigned stoppedWhen;

	TestStarter() : started(false), startedWhen(0), stoppedWhen(0) {};
		
	virtual void startableStart()
	{
		started = true;
		startedWhen = ++seq;
	}
	virtual void startableStop()
	{
		started = false;
		stoppedWhen = ++seq;
	}
};

unsigned TestStarter::seq = 0;

// Check that reset also stops the starters
template<> template<>
void to::test<1>()
{
	TestStarter first;

	Starter& s = Starter::get();
	s.reset();

	s.add(&first, 10);

	s.start();

	ensure(first.started);

	s.reset();

	ensure(!first.started);
}

// Check that start and stop happen, and in order
template<> template<>
void to::test<2>()
{
	TestStarter first, second, third;

	Starter& s = Starter::get();
	s.reset();

	// Add the starters
	s.add(&third, 30u);
	s.add(&second, 20);
	s.add(&first, 10);

	// Start everything
	s.start();

	// Ensure that everything is started
	ensure(first.started);
	ensure(second.started);
	ensure(third.started);

	// Ensure that everything is started in the right order
	ensure(first.startedWhen < second.startedWhen);
	ensure(second.startedWhen < third.startedWhen);

	// Stop everything
	s.stop();

	// Ensure that everything is stopped
	ensure(!first.started);
	ensure(!second.started);
	ensure(!third.started);

	// Ensure that everything is stopped in the right order
	ensure(third.stoppedWhen < second.stoppedWhen);
	ensure(second.stoppedWhen < first.stoppedWhen);
}

// Check that adding a Startable when the Starter is started, starts it.
template<> template<>
void to::test<3>()
{
	TestStarter first, second, third;

	Starter& s = Starter::get();
	s.reset();

	s.add(&second, 20);

	s.start();

	ensure(!first.started);
	ensure(second.started);
	ensure(!third.started);

	s.add(&first, 10);

	ensure(first.started);
	ensure(second.started);
	ensure(!third.started);

	s.stop();

	ensure(!first.started);
	ensure(!second.started);
	ensure(!third.started);

	s.add(&third, 10);

	ensure(!first.started);
	ensure(!second.started);
	ensure(!third.started);
}

}

// vim:set ts=4 sw=4:
