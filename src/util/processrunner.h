#ifndef GUESSNET_UTIL_PROCESS_RUNNER_H
#define GUESSNET_UTIL_PROCESS_RUNNER_H

/*
 * Run scripts in parallel
 *
 * Copyright (C) 2003--2007  Enrico Zini <enrico@debian.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <wibble/exception.h>
#include <wibble/sys/thread.h>
#include <wibble/sys/mutex.h>
#include "starter.h"
#include <string>
#include <vector>
#include <queue>

class ProcessListener
{
public:
	virtual ~ProcessListener() {}
	virtual void handleTermination(const std::string& tag, int status) {}
};


namespace processrunner
{
class Script;

/**
 * Information that we keep about a process
 */
struct ProcData
{
	std::string tag;
	processrunner::Script* script;
	ProcessListener* listener;

	ProcData(
		const std::string& tag,
		const std::string& cmdline,
		const std::vector<std::string>& env,
		ProcessListener* listener);
    ProcData(const ProcData&);
    ~ProcData();

	void run();

private:
    ProcData& operator=(const ProcData&);
};

}

/*
 * Injects ethernet packets to a given interface.
 *
 * Every method is thread-safe
 */
class ProcessRunner : public util::Startable, public wibble::sys::Thread
{
protected:
	wibble::sys::Mutex listenersMutex;
	wibble::sys::Condition listenersCond;

	std::queue<processrunner::ProcData> queuedForRunning;
	bool requested_shutdown;
	bool running;

	virtual void* main();
	
	ProcessRunner();

public:
	~ProcessRunner();

	/**
	 * Enqueue a process for running.
	 *
	 * Tag is a tag that will be notified to the listener
	 * Cmdline is a string with the command line to run.  It will be run using "sh -c"
	 * Env is a list of "VAR=value" that defines the environment.  No
	 *  environment variables are copied from the parent process.
	 */
	void addProcess(const std::string& tag,
	                const std::string& cmdline,
					const std::vector<std::string>& env,
					ProcessListener* pl);

	void shutdown();

	virtual void startableStart() { start(); /* start the thread */ }
	virtual void startableStop() { shutdown(); }

	/**
	 * Get the singleton ProcessRunner instance.
	 *
	 * The instance is automatically registered with the starter.
	 */
	static ProcessRunner& get();

	/**
	 * Reset the singleton instance.
	 *
	 * This is useful only for the tests, that may need to test registering the
	 * processrunner with a starter multiple times.
	 *
	 * Note that this *must* follow a reset of the Starter, otherwise the
	 * starter will be left with a dangling pointer.
	 */
	static void reset();
};

// vim:set ts=4 sw=4:
#endif
