#ifndef GUESSNET_UTIL_STARTER_H
#define GUESSNET_UTIL_STARTER_H

/*
 * Coordinate starting and stopping of subsystems
 *
 * Copyright (C) 2007  Enrico Zini <enrico@debian.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 */

#include <map>
#include <list>
#include <limits>

namespace util {

/**
 * Interface for subsystems that can be started and stopped
 */
struct Startable
{
	virtual ~Startable() {}
	virtual void startableStart() = 0;
	virtual void startableStop() = 0;
};

class Starter
{
	std::map< unsigned, std::list<Startable*> > members;
	bool started;

	Starter();

public:
	/**
	 * Add a Startable to be started and stopped by this Starter.
	 *
	 * If start() has already been called, the Started will also be started.
	 */
	void add(Startable* s, unsigned prio = std::numeric_limits<unsigned>::max());

	/**
	 * Start all the Startables, in increasing priority order
	 */
	void start();

	/**
	 * Stop all the Startables, in the opposite order as start
	 */
	void stop();

	/**
	 * Remove all the Startables from the Starter.
	 *
	 * If they are running, they are stopped before removal.
	 */
	void reset();

	/**
	 * Singleton access instance
	 */
	static Starter& get();
};

}

// vim:set ts=4 sw=4:
#endif
