#ifndef GUESSNET_UTIL_PACKET_MAKER_H
#define GUESSNET_UTIL_PACKET_MAKER_H

#include <wibble/sys/buffer.h>
#include "nettypes.h"

namespace util {

struct PacketMaker
{
	static wibble::sys::Buffer makeARPRequest(const IPAddress& ip, const IPAddress& src);
	static wibble::sys::Buffer makeARPRequest(const ether_addr& mac, const IPAddress& src);
	static wibble::sys::Buffer makePingRequest(const ether_addr& mac, const IPAddress& src);
	static wibble::sys::Buffer makeDHCPRequest();
};

}

// vim:set ts=4 sw=4:
#endif
