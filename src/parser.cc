/*
 * Common facilities used by test data parsers
 *
 * Copyright (C) 2003  Enrico Zini <enrico@debian.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "parser.h"
#include <sstream>

namespace wibble {
namespace exception {

static std::string parserContext(const std::string& file, int line) throw ()
{
	std::stringstream str;
	if (line == -1)
		str << "parsing file " << file;
	else
		str << file << ":" << line;
	return str.str();
}

static std::string parserContext(int line) throw ()
{
	std::stringstream str;
	str << "parsing line " << line;
	return str.str();
}

Parser::Parser(const std::string& file, int line, const std::string& error) throw ()
	: Consistency(parserContext(file, line), error), m_file(file), m_line(line) {}
Parser::Parser(int line, const std::string& error) throw ()
	: Consistency(parserContext(line), error), m_line(line) {}
Parser::Parser(const std::string& context, const std::string& error) throw ()
	: Consistency(context, error), m_line(-1) {}

void Parser::setLocation(const std::string file, int line) throw ()
{
	m_file = file;
	if (line != -1) m_line = line;
	m_context[0] = parserContext(m_file, m_line);
}

void Parser::setLocation(int line) throw ()
{
	if (line != -1) m_line = line;
	m_context[0] = parserContext(m_file, m_line);
}

}
}

// vim:set ts=4 sw=4:
