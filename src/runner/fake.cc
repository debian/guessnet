/*
 * Run scans and arbitrate the results
 *
 * Copyright (C) 2003--2007  Enrico Zini <enrico@debian.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "runner/fake.h"
#include <iostream>
#include <cstdio>

using namespace std;
using namespace scanner;
using namespace wibble::sys;

namespace runner {

void Fake::startScans()
{
	printf("Scans contents:\n");
	ScanBag::get().dump();
	
	/*
	for (list<const Scan*>::const_iterator i = scanbag.getScans().begin();
			i != scanbag.getScans().end(); i++)
	{
		const Scan* scan = *i;
		if (const PeerScan* s = dynamic_cast<const PeerScan*>(scan))
		{
			debug("Will check network %s for IP address %s (MAC %s)\n",
					s->name().c_str(), fmt(s->ip()).c_str(), fmt(s->mac()).c_str());
			cand_count++;
		}
		else if (const LinkBeatScan* s = dynamic_cast<const LinkBeatScan*>(scan))
		{
			//warning("Link-beat detection currently disabled\n");
			debug("Will test for link beat.  If absent, will return %s\n",
					s->name().c_str());
			cand_count++;
		}
		else if (const ScriptScan* s = dynamic_cast<const ScriptScan*>(scan))
		{
			debug("Will use command '%s' to test %s\n",
					s->cmdline().c_str(), s->name().c_str());
			cand_count++;
		}
		if (const DHCPScan* s = dynamic_cast<const DHCPScan*>(scan))
		{
			debug("Will check network %s for DHCP service\n",
					s->name().c_str());
			cand_count++;
		}
		else if (const DefaultScan* s = dynamic_cast<const DefaultScan*>(scan))
		{
			debug("Default test is %s\n",
					s->name().c_str());
			cand_count++;
		}
		else
			//printf("Unknown test %p\n", scan);
			printf("Unknown test %s: %s\n", scan->name().c_str(), scan->signature().c_str());
	}
	*/

	start();
}

void Fake::interact()
{
	FILE* in = fopen("/dev/tty", "rt");

	// To be run in a separate thread
	while (!_canceled)
	{
		int num = 1;
		const list<Scan*>& cands = ScanBag::get().getScans();
		for (list<Scan*>::const_iterator i = cands.begin();
				i != cands.end(); i++)
			cout << num++ << ": " << (*i)->name() << " (" << (*i)->signature() << ")" << endl;
		cout << " 0: quit" << endl;
		cout << "> ";
		unsigned int res;
		fscanf(in, "%d", &res);
		if (res == 0)
			canceled(true);
		res--;
		if (res > 0 && res < cands.size())
		{
			list<Scan*>::const_iterator i = cands.begin();
			for (unsigned int j = 0; j < res; j++)
				i++;
			(*i)->success();
		}
	}
	fclose(in);
}

}

// vim:set ts=4 sw=4:
