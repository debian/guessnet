#ifndef GUESSNET_RUNNER_FAKE_H
#define GUESSNET_RUNNER_FAKE_H

/*
 * Run scans and arbitrate the results
 *
 * Copyright (C) 2003--2007  Enrico Zini <enrico@debian.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string>
#include <wibble/exception.h>
#include <wibble/sys/thread.h>
#include "runner/runner.h"

namespace runner {

/**
 * Fake runner: ask the user if tests should pass instead of running them
 */
class Fake : public Runner, wibble::sys::Thread
{
protected:
	bool _canceled;
	
	virtual void* main()
	{
		interact();
		return 0;
	}
	
public:
	Fake() : _canceled(false) {}
	~Fake() {}

	bool canceled() const { return _canceled; }
	bool canceled(bool val) { return _canceled = val; }
	
	void shutdown() { canceled(true); }

	void startScans();

	void interact();
};

}

// vim:set ts=4 sw=4:
#endif
