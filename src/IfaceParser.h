#ifndef IFACE_PARSER_H
#define IFACE_PARSER_H

/*
 * /etc/network/interfaces parser
 *
 * Copyright (C) 2003--2010  Enrico Zini <enrico@debian.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "parser.h"
#include "options.h"
#include <stdio.h>

class IfaceParser
{
public:
	static void parseMapping(FILE* in);
	static void parse(FILE* in, const IfaceFilter& filter = IfaceFilter());
};

// vim:set ts=4 sw=4:
#endif
