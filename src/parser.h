#ifndef PARSER_H
#define PARSER_H

/*
 * Common facilities used by test data parsers
 *
 * Copyright (C) 2003  Enrico Zini <enrico@debian.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string>
#include <wibble/exception.h>
#include "scanner/scan.h"
#include "nettypes.h"

namespace wibble {
namespace exception {

class Parser: public Consistency
{
protected:
	std::string m_file;
	int m_line;

public:
	Parser(const std::string& file, int line, const std::string& error) throw ();
	Parser(int line, const std::string& error) throw ();
	Parser(const std::string& context, const std::string& error) throw ();
	~Parser() throw () {}

	int line() const throw () { return m_line; }
	const std::string& file() const throw () { return m_file; }
	std::string file() throw () { return m_file; }

	void setLocation(const std::string file, int line = -1) throw ();
	void setLocation(int line) throw ();

	virtual const char* type() const throw () { return "Parser"; }

};

}
}


// vim:set ts=4 sw=4:
#endif
