/*
 * Copyright (C) 2007--2008  Enrico Zini <enrico@enricozini.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "tests/test-utils.h"
#include "scanner/scanbag.h"
#include "scanner/scan.h"
#include "scanner/linkbeat.h"
#include "scanner/script.h"

#include <memory>

namespace tut {
using namespace std;
using namespace scanner;

struct scanner_scanbag_shar {
	Scan* scan_default;

	scanner_scanbag_shar()
	{
		scan_default = new DefaultScan("default");
	}
	~scanner_scanbag_shar()
	{
		delete scan_default;
	}
};
TESTGRP(scanner_scanbag);

void printCands(const vector<string>& v)
{
	for (vector<string>::const_iterator i = v.begin(); i != v.end(); i++)
		if (i != v.begin())
			cout << ", " << *i;
		else
			cout << *i;
}

// Test the case with no scans besides the default
template<> template<>
void to::test<1>()
{
	ScanBag& sb = ScanBag::get();
	sb.clear();
	sb.add(scan_default);

	ensure(!sb.anySuccess());
	ensure_equals(sb.getFinal(), string());
}

// Test the case only one scan besides the default
template<> template<>
void to::test<2>()
{
	ScanBag& sb = ScanBag::get();
	sb.clear();
	sb.add(scan_default);

	auto_ptr<Scan> scan(Script::createScan("test0", "cmd-test0"));
	sb.add(scan.get());

	ensure(!sb.anySuccess());
	ensure_equals(sb.getFinal(), string());

	// Upon success, we should get it
	sb.notifySuccess(scan.get());
	ensure(sb.anySuccess());
	ensure_equals(sb.getFinal(), "test0");

	// Success could however be notified twice, without invalidating the
	// results
	sb.notifySuccess(scan.get());
	ensure(sb.anySuccess());
	ensure_equals(sb.getFinal(), "test0");
}

// Messy scanbag test from before we had a proper test suite
template<> template<>
void to::test<3>()
{
	ScanBag& sb = ScanBag::get();

	Scan* s20 = LinkBeat::createScan("default");
	Scan* s2 = LinkBeat::createScan("nolink");
	Scan* s21 = Script::createScan("test0", "cmd-test0");
	Scan* s3 = Script::createScan("test1", "cmd-test1");
	Scan* s4 = Script::createScan("test2", "cmd-test2");
	Scan* s5 = Script::createScan("test1", "cmd-test2");
	Scan* s6 = Script::createScan("test3", "cmd-test3");

	sb.clear();
	sb.add(scan_default);
	sb.add(s20);
	sb.add(s2);
	sb.add(s21);
	sb.add(s3);
	sb.add(s4);
	sb.add(s5);
	sb.add(s6);

	ensure(!sb.anySuccess());
	ensure_equals(sb.getFinal(), string());
	ensure_equals(sb.getLessSpecific(), "default");

	vector<string> v;

	sb.notifySuccess(s21);
	v = sb.getRemaining();
	ensure_equals(v.size(), 1);
	ensure_equals(v[0], "test0");

	sb.clear();
	sb.add(scan_default);
	sb.add(s20);
	sb.add(s2);
	sb.add(s21);
	sb.add(s3);
	sb.add(s4);
	sb.add(s5);
	sb.add(s6);

	ensure(!sb.anySuccess());
	ensure_equals(sb.getFinal(), string());

	sb.notifySuccess(s4);
	//printf("Notified %.*s: ", PFSTR(s4->signature())); printCands(v); printf("\n");

	sb.notifySuccess(s3);
	//printf("Notified %.*s: ", PFSTR(s3->signature())); printCands(v); printf("\n");

	v = sb.getRemaining();
	ensure_equals(v.size(), 1);
	ensure_equals(v[0], "test1");

	sb.clear();
	sb.add(scan_default);
	sb.add(s20);
	sb.add(s2);
	sb.add(s21);
	sb.add(s3);
	sb.add(s4);
	sb.add(s5);
	sb.add(s6);

	sb.notifySuccess(s4);
	v = sb.getRemaining();
	ensure_equals(sb.getLessSpecific(), "test2");

	delete s20;
	delete s2;
	delete s21;
	delete s3;
	delete s4;
	delete s5;
	delete s6;
}

// Test what happens with a duplicate profile
template<> template<>
void to::test<4>()
{
	ScanBag& sb = ScanBag::get();
	sb.clear();
	sb.add(scan_default);

	auto_ptr<Scan> scan(Script::createScan("test0", "cmd-test0"));
	sb.add(scan.get());

	auto_ptr<Scan> scan1(Script::createScan("test0", "cmd-test1"));
	sb.add(scan1.get());

	auto_ptr<Scan> scan2(Script::createScan("test1", "cmd-test0"));
	sb.add(scan2.get());

	ensure(!sb.anySuccess());
	ensure_equals(sb.getFinal(), string());

	// Report success on the common test
	sb.notifySuccess(scan.get());
	ensure(sb.anySuccess());
	ensure_equals(sb.getFinal(), string());

	// Report success on the other test
	sb.notifySuccess(scan1.get());
	ensure(sb.anySuccess());
	ensure_equals(sb.getFinal(), "test0");
}

}

// vim:set ts=3 sw=3:
