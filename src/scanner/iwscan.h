#ifndef GUESSNET_SCANNER_IWSCAN_H
#define GUESSNET_SCANNER_IWSCAN_H

/*
 * Perform a wireless interface scan
 *
 * Copyright (C) 2007  Enrico Zini <enrico@debian.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <wibble/sys/mutex.h>
#include <wibble/sys/thread.h>

#include "scanner/scan.h"
#include "util/starter.h"
#include "IFace.h"
#include "nettypes.h"

#include <list>

struct wireless_scan;

namespace scanner {

class WirelessScan : public Scan
{
protected:
	struct ether_addr _mac;
	std::string _essid;
	bool _hasOpen;
	bool _open;

public:
	WirelessScan(const std::string& name);

	/**
	 * After all the fields have been set, call this function to finalise the
	 * creation of the scan
	 */
	void finaliseInit();

	const ether_addr& mac() const { return _mac; }
	const std::string& essid() const { return _essid; }
	bool open() const { return _open; }

	void setMAC(const ether_addr& mac) { _mac = mac; }
	void setESSID(const std::string& essid) { _essid = essid; }
	void setOpen(bool open) { _hasOpen = true; _open = open; }

	bool hasMAC() const;
	bool hasESSID() const { return !_essid.empty(); }
	bool hasOpen() const { return _hasOpen; }

	/**
	 * Return true if this scan matches the given scan data
	 */
	bool matches(const wireless_scan& data);

	virtual std::string signature() const;
};


class IWScan : public wibble::sys::Thread, public util::Startable
{
protected:
	std::string iface;
	std::list<WirelessScan*> candidates;

	virtual void* main();
	
	IWScan(const std::string& iface);

public: 
	virtual ~IWScan();

	void startableStart() { start(); /* Start the thread */ }
	void startableStop() { join(); }

	// Don't call this after start
	void addCandidate(WirelessScan* scan);

	static void configure(const std::string& iface);
	static IWScan& get();
};

}

// vim:set ts=4 sw=4:
#endif
