#include "scanner/scan.h"
#include "scanner/scanbag.h"

#include <strings.h>

using namespace std;

namespace scanner {

void Scan::success()
{
	ScanBag::get().notifySuccess(this);
}

string DefaultScan::signature() const
{
	return "default";
}

}


#ifdef COMPILE_TESTSUITE

#include <tests/test-utils.h>

namespace tut {
using namespace tut_guessnet;

struct guessnet_scans_shar {
};
TESTGRP(guessnet_scans);

template<> template<>
void to::test<1>()
{
	PeerScan peer("test");

	gen_ensure(peer.name() == "test");
	//ensure_equals(peer.name(), "test");
	gen_ensure(!peer.hasIP());
	gen_ensure(!peer.hasMAC());
	gen_ensure(!peer.hasSource());

	peer.setIP(IPAddress("1.2.3.4"));
	gen_ensure(peer.hasIP());
	gen_ensure(!peer.hasMAC());
	gen_ensure(!peer.hasSource());

	peer.setSource(IPAddress("10.20.30.40"));
	gen_ensure(peer.hasIP());
	gen_ensure(!peer.hasMAC());
	gen_ensure(peer.hasSource());

	struct ether_addr testmac;
	parse_mac(&testmac, "11:22:33:44:55:66");
	peer.setMAC(testmac);
	gen_ensure(peer.hasIP());
	gen_ensure(peer.hasMAC());
	gen_ensure(peer.hasSource());

	gen_ensure(peer.ip() == IPAddress("1.2.3.4"));
	//ensure_equals(peer.ip(), IPAddress("1.2.3.4"));
	gen_ensure(MAC_MATCHES(&(peer.mac()), &testmac));
	gen_ensure(peer.source() == IPAddress("10.20.30.40"));
	//ensure_equals(peer.source(), IPAddress("10.20.30.40"));
}

}

#endif

// vim:set ts=3 sw=3:
