#ifndef TRAFFIC_SCANNER_H
#define TRAFFIC_SCANNER_H

/*
 * Sniff network traffic to guess network data
 *
 * Copyright (C) 2003  Enrico Zini <enrico@debian.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <wibble/sys/mutex.h>
#include "Scanner.h"
#include "NetWatcher.h"
#include "NetSender.h"
#include "nettypes.h"

#include <map>
#include <set>

class TrafficScanner : public PacketListener
{
public:
	typedef long long int mac_key;
	typedef unsigned int ip_key;

	struct HostData
	{
		ip_key addr;
		std::set<ip_key> addr_sent;
		std::set<ip_key> addr_recv;

		HostData() throw () : addr(0) {}
	};

	std::map<mac_key, HostData> scanData;
	std::map<ip_key, mac_key> local_addrs;

	ip_key guessed_netaddr;
	std::set<mac_key> guessed_gateways;
	
protected:
	NetSender sender;

	void requestArp(ip_key addr) throw ();

public: 
	TrafficScanner(NetSender sender) throw ()
		: guessed_netaddr(0xffffffff), sender(sender) {}

	virtual void handleEthernet(struct libnet_ethernet_hdr* eth_header) throw ();
};

// vim:set ts=4 sw=4:
#endif
