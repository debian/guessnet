#ifndef GUESSNET_SCANNER_PEER_H
#define GUESSNET_SCANNER_PEER_H

/*
 * Scan for the existance of a specific peer using fake ARP requests
 *
 * Copyright (C) 2003--2007  Enrico Zini <enrico@debian.org>
 * Originally based on laptop-netconf.c by Matt Kern <matt@debian.org>
 * Which in turn was nased on divine.c by Felix von Leitner <felix@fefe.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "nettypes.h"
#include "scanner/scan.h"
#include "util/netwatcher.h"

#include <wibble/sys/netbuffer.h>

namespace scanner {

struct Scan;

/*
 * Peer test
 *
 * Succeeds if host replies to ARP request
 */
class PeerScan : public Scan, public util::PacketListener, public util::Startable
{
protected:
	struct ether_addr _mac;
	IPAddress _ip;
	IPAddress _source;

public:
	PeerScan(const std::string& name);

	PeerScan(const std::string& name, const ether_addr& mac, const IPAddress& ip)
		: Scan(name), _mac(mac), _ip(ip), _source("0.0.0.0") {}

	PeerScan(const std::string& name, const ether_addr& mac, const IPAddress& ip, const IPAddress& source)
		: Scan(name), _mac(mac), _ip(ip), _source(source) {}

	/**
	 * After all the fields have been set, call this function to finalise the
	 * creation of the scan
	 */
	void finaliseInit();

	void startableStart();
	void startableStop();

	const ether_addr& mac() const { return _mac; }
	const IPAddress& ip() const { return _ip; }
	const IPAddress& source() const { return _source; }

	void setMAC(const ether_addr& mac) { _mac = mac; }
	void setIP(const IPAddress& ip) { _ip = ip; }
	void setSource(const IPAddress& source) { _source = source; }

	bool hasMAC() const;
	bool hasIP() const { return _ip != IPAddress("0.0.0.0"); }
	bool hasSource() const { return _source != IPAddress("0.0.0.0"); }

	void handleEthernet(const wibble::sys::NetBuffer& pkt);
	void handleARP(const wibble::sys::NetBuffer& arp);

	virtual std::string signature() const;
};


struct Peer
{
	static Scan* createScan(const std::string& name, const ether_addr& mac, const IPAddress& ip);
	static Scan* createScan(const std::string& name, const ether_addr& mac, const IPAddress& ip, const IPAddress& source);
};

}

// vim:set ts=4 sw=4:
#endif
