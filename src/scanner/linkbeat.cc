/*
 * Test for link beat
 *
 * Copyright (C) 2003--2010  Enrico Zini <enrico@debian.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "scanner/linkbeat.h"
#include "scanner/scan.h"
#include "util/output.h"
#include "util/starter.h"
#include "IFace.h"
#include "options.h"

#include <memory>

using namespace std;

namespace scanner {

/*
 * Link beat test
 *
 * Succeeds if no link beat reported
 */
struct LinkBeatScan : public Scan, public util::Startable
{
	static IFace* iface;

	LinkBeatScan(const std::string& name)
		: Scan(name) {}
	virtual ~LinkBeatScan() {}

	void startableStart()
	{
		//warning("Link-beat detection currently disabled\n");
		debug("Will test for link beat.  If absent, will return %s\n", name().c_str());

		iface->update();
		if (!iface->has_mii())
			warning("Link beat test requested on an interface without link beat detection support: assuming we are always connected\n");
		else if (!iface->connected())
		{
			verbose("Link beat not detected\n");
			success();
		}
	}

	void startableStop() {}

	virtual std::string signature() const
	{
		return "linkbeat";
	}
};

IFace* LinkBeatScan::iface = 0;

void LinkBeat::configure(IFace* iface)
{
	LinkBeatScan::iface = iface;
}
		
Scan* LinkBeat::createScan(const std::string& name)
{
	auto_ptr<LinkBeatScan> res(new LinkBeatScan(name));

	verbose("adding candidate script LinkBeat with tag [%s]\n", res->signature().c_str());
	util::Starter::get().add(res.get(), 10);

	return res.release();
}

}

// vim:set ts=4 sw=4:
