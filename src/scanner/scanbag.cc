/*
 * Copyright (C) 2005--2008  Enrico Zini <enrico@enricozini.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "scanbag.h"
#include "util/output.h"

#include <sstream>

using namespace std;
using namespace wibble::sys;
using namespace scanner;

static ScanBag* instance = 0;

ScanBag& ScanBag::get()
{
	if (instance == 0)
		instance = new ScanBag;
	return *instance;
}


void ScanBag::add(Scan* scan)
{
	iterator i = find(scan->name());

	if (i == end())
	{
		if (DefaultScan* s = dynamic_cast<DefaultScan*>(scan))
		{
			//insert(pair<string, set<string> >(scan->name(), set<string>()));
			defaultProfile = s->name();
		} else {
			map<string, bool> v;
			v.insert(make_pair(scan->signature(), false));
			insert(make_pair(scan->name(), v));
		}
	}
	else
		i->second.insert(make_pair(scan->signature(), false));

	scans.push_back(scan);
}

void ScanBag::notifySuccess(Scan* scan)
{
	bool changed = false;
	{
		MutexLock lock(mutex);

		debug("Notified success of scan %s\n", scan->signature().c_str());

		any_success = true;

		// The candidate winnowing algorithm:
		// For every candidate profile: if the profile has the scan
		// with the signature of "scan" then remove that scan;
		// otherwise remove the candidate.
		// Return the list of candidates left.

		//defaultProfile = "";
		
		string sig = scan->signature();
		for (iterator i = begin(); i != end(); )
		{
			map<string, bool>::iterator j = i->second.find(sig);
			if (j == i->second.end())
			{
				debug("Removing candidate %s\n", i->first.c_str());
				// This candidate has no scan with the signature
				// so remove the candidate
				iterator k = i;
				k++;
				erase(i);
				i=k;
				changed = true;
			}
			else
			{
				debug("Keeping candidate %s\n", i->first.c_str());
				// This candidate has a scan with the signature
				// so remove the matching scan
				j->second = true;
				i++;
			}
		}
	}

	// Notify that the list of candidates changed
	if (changed && listener)
	{
		debug("We had changes, notifying the listener\n");
		listener->candidatesChanged();
	}
}

bool ScanBag::anySuccess()
{
	MutexLock lock(mutex);
	return any_success;
}

std::string ScanBag::getFinal()
{
	MutexLock lock(mutex);
	if (any_success && size() == 1)
		return begin()->first;
	return string();
}

string ScanBag::getLessSpecific()
{
	MutexLock lock(mutex);

	string cand;
	int count = -1;
	for (const_iterator i = begin(); i != end(); i++)
	{
		int unfired = 0;
		for (map<string, bool>::const_iterator j = i->second.begin();
				j != i->second.end(); ++j)
			if (!j->second)
				++unfired;

		if (count == -1 || unfired < count)
		{
			count = unfired;
			cand = i->first;
		}
	}
	return cand;
}

string ScanBag::getDefault()
{
	MutexLock lock(mutex);

	return defaultProfile;
}

std::vector<std::string> ScanBag::getRemaining()
{
	MutexLock lock(mutex);

	vector<string> res;
	for (iterator i = begin(); i != end(); ++i)
	{
		res.push_back(i->first);
	}
	return res;
}

void ScanBag::dump()
{
	MutexLock lock(mutex);

	stringstream str;
	for (const_iterator i = begin(); i != end(); i++)
	{
		str << i->first << ": ";
		for (map<string, bool>::const_iterator j = i->second.begin(); j != i->second.end(); j++)
		{
			if (j != i->second.begin())
				str << ", ";
			str << j->first << (j->second ? "(ok)" : "");
		}
		str << endl;
	}
	output("%s", str.str().c_str());
}

// vim:set ts=3 sw=3:
