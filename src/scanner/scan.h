#ifndef GUESSNET_SCANNER_SCAN_H
#define GUESSNET_SCANNER_SCAN_H

#include <string>
#include <vector>

namespace scanner {

/*
 * Scan
 */
class Scan
{
protected:
	std::string _name;

public:
	Scan(const std::string& name) : _name(name) {}
	virtual ~Scan() {}

	/// Notify success for this scan
	void success();
	
	const std::string& name() const { return _name; }

	virtual std::string signature() const = 0;
};

/*
 * Default test
 *
 * Always succeeds
 */
class DefaultScan : public Scan
{
public:
	DefaultScan(const std::string& name) : Scan(name) {}

	virtual std::string signature() const;
};

}

// vim:set ts=3 sw=3:
#endif
