/*
 * Scan for the existance of a DHCP server
 *
 * Copyright (C) 2004--2007  Enrico Zini <enrico@debian.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "scanner/dhcp.h"
#include "scanner/scan.h"
#include "util/packetmaker.h"
#include "util/netsender.h"
#include "util/netwatcher.h"
#include "util/output.h"

#include <netinet/in.h> // ntohs, htons, ...

#include <memory>

/*
extern "C" {
#include <libnet.h>
}
*/

using namespace std;
using namespace wibble::sys;
using namespace util;

namespace scanner {

/*
 * DHCP test
 *
 * Succeeds if a DHCP server makes an offer
 */
class DHCPScan : public Scan, public PacketListener, public Startable
{
public:
	DHCPScan(const std::string& name)
		: Scan(name) {}

	void startableStart()
	{
		// Build and send the DHCP request
		Buffer pkt = PacketMaker::makeDHCPRequest();

		verbose("Sending 5 DHCP probes, 1 every 2 seconds...\n");

		// Enqueue the probe packets for sending
		NetSender& sender = NetSender::get();

		// Enqueue the packet for sending
		sender.post(pkt, 2000, 10000);
	}

	void startableStop() {}

	void handleDHCP(const NetBuffer& dhcp);

	virtual std::string signature() const
	{
		return "dhcp";// + fmt(ip()) + " " + fmt(mac());
	}
};

void DHCPScan::handleDHCP(const NetBuffer& dhcp)
{
	const libnet_dhcpv4_hdr* dhcp_header = dhcp.cast<libnet_dhcpv4_hdr>();

	// Parse and check the DHCP header
	if (dhcp_header->dhcp_opcode == LIBNET_DHCP_REPLY)
	{
		debug("Got DHCP reply\n");
#if 0
		//in_addr* ipv4_him = arp_get_tip(arp_header);
		in_addr* ipv4_him = arp_get_sip(arp_header);
		ether_addr* mac_him = arp_get_sha(arp_header);

		debug("Got ARP reply from %.*s %.*s\n", PFSTR(fmt(IPAddress(*ipv4_him))), PFSTR(fmt(*mac_him)));

		//IPv4_FROM_LIBNET(ipv4_me, arp_header->ar_tpa);
		//IPv4_FROM_ARP(ipv4_him, arp_header->ar_spa);
#endif

#if 0
		// Check if IP and MAC addresses match
		if (IPv4_MATCHES(&(*i)->ip(), ipv4_him))
		{
			if (MAC_MATCHES(&(*i)->mac(), mac_him))
			{
				debug("ARP reply from %.*s %.*s matches\n", PFSTR(fmt(IPAddress(*ipv4_him))), PFSTR(fmt(*mac_him)));
				succeeded(*i);
			}
			else
			{
				// If only the IP matches, check if the test is IP-only
				struct ether_addr zeroAddr;
				bzero(&zeroAddr, sizeof(struct ether_addr));
				if (MAC_MATCHES(&(*i)->mac(), &zeroAddr))
				{
					debug("ARP reply from %.*s %.*s matches\n", PFSTR(fmt(IPAddress(*ipv4_him))), PFSTR(fmt(*mac_him)));
					succeeded(*i);
				}
			}
		}
#endif
		success();
	}
}

Scan* DHCP::createScan(const std::string& name)
{
	auto_ptr<DHCPScan> res(new DHCPScan(name));

	// Register with the NetWatcher
	NetWatcher& watcher = NetWatcher::get();
	watcher.addDHCPListener(res.get());

	// Access the NetSender to show that we need it
	NetSender& sender = NetSender::get();

	return res.release();
}

}
// vim:set ts=4 sw=4:
