#ifndef OPTIONS_H
#define OPTIONS_H

/*
 * Program options
 *
 * Copyright (C) 2003--2010  Enrico Zini <enrico@debian.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string>
#include <vector>
#include "parser.h"

namespace wibble {
namespace commandline {
struct GuessnetOptions;
}
}

/**
 * Base stanza filter, to decide what we want and what we don't
 */
struct IfaceFilter
{
	virtual ~IfaceFilter() {}
	virtual bool operator()(const std::string& name) const { return true; }
};


class Options
{
protected:
	void init_standalone(wibble::commandline::GuessnetOptions& opts);
	void init_ifupdown(wibble::commandline::GuessnetOptions& opts);
	void parse_guessnet_config();
	void parse_ifupdown_config();

public:
	/// Default iterface to use
	std::string iface;

	/// Default tag to print when nothing is found
	std::string defprof;

	/// Timeout after which we decide we haven't found anything
	int timeout;

	/// Time we wait after initializing an interface
	int init_timeout;

	/// Filter in/out profiles based on their names
	bool autofilter;

	/// Initial delay to set in case of race conditions
	int initdelay;

	/// Number of tries for wireless scanning
	int iwscan_tries;

	/// Configuration file
	std::string config_file;

	/**
	 * Filter that decides which tests we select from the configuration
	 * file
	 */
	IfaceFilter* iface_filter;

	/// List of tests to perform
	std::vector<const scanner::Scan*> scans;

	Options();
	~Options();

	/**
	 * Initialise options from any source implied from command line
	 */
	void init(int argc, const char* argv[]);
};

extern Options options;

// vim:set ts=4 sw=4:
#endif
