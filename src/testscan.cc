/*
 * Guess the current network location
 *
 * Copyright (C) 2003  Enrico Zini <enrico@debian.org>
 * Mostly rewritten by Enrico Zini on May 2003
 * Originally based on laptop-netconf.c by Matt Kern <matt@debian.org>
 * That was in turn based on divine.c by Felix von Leitner <felix@fefe.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <iwlib.h>
#include <stdio.h>
#if 0
#include <ctype.h>
#include <errno.h>	/* errno */

#include <string.h>     // memcpy
#include <sys/types.h>  // socket, getpwuid, geteuid
#include <sys/socket.h> // socket
#include <sys/ioctl.h>  // ioctl
#include <net/if.h>
#include <unistd.h>     // close, geteuid
#include <pwd.h>		// getpwuid
#endif

using namespace std;

int main (int argc, const char *argv[])
{
	// Access the interface
	const char* iface = argv[1];

	// Apre il socket per parlare con il supporto di rete del kernel
	int skfd = iw_sockets_open();
	if (skfd < 0)
	{
		perror("socket");
		exit(1);
	}

	// Questo serve a iw_scan e iw_scan lo vuole passato perché non vuole
	// ricalcolarselo ogni volta
	int we_version = iw_get_kernel_we_version();

	// Fa lo scan, bloccante
	//(iw_process_scan è la non blocking)
	wireless_scan_head scan_context;
	if (iw_scan(skfd, (char*)iface, we_version, &scan_context) < 0)
	{
		perror("iw_scan");
		exit(1);
	}

	for (wireless_scan* i = scan_context.result; i != 0; i = i->next)
	{
		printf("name %s essid %s\n", i->b.name, i->b.essid);
	}

	// TODO: deallocare la lista

	return 0;
}

// vim:set ts=4 sw=4:
