# NOTE: this file presents a few configurations in order to show various
# applications of guessnet options. Do not use it as-is, just take some
# hints and write your own. This file is not guaranteed to work as-is, 
# and it probably won't anyway. :P
auto lo

iface lo inet loopback

auto eth0

mapping eth0
	script /usr/sbin/guessnet-ifupdown
	# List of stanzas guessnet should scan for
	#   If none is specified, scans for all stanzas
	#map home work
	map default: dhcp
	map timeout: 3
	map verbose: true

# Home network configuration
iface home inet static
	address 192.168.1.2
	netmask 255.255.255.0
	broadcast 192.168.1.255
	gateway 192.168.1.1
	dns-search home.loc
	dns-nameservers 192.168.1.1
	# Check for one of these hosts:
	test1-peer address 192.168.1.1 mac 00:01:02:03:04:05
	test2-peer address 192.168.1.3 mac 00:01:02:03:04:06

# Work network configuration
iface work inet static
	address 10.1.1.42
	netmask 255.255.255.0
	broadcast 10.1.1.255
	gateway 10.1.1.1
	dns-search work.loc
	dns-nameservers 10.1.1.1
	# the other guessnet scan:
	test-command /usr/local/bin/check_work

# Second job network configuration
iface work2 inet static
	address 192.168.2.23
	netmask 255.255.255.0
	broadcast 192.168.2.255
	gateway 192.168.2.1
	dns-search work2.loc
	dns-nameservers 192.168.2.1
	# Specify a source address in case the peer doesn't reply to
        # ARP packets coming from 0.0.0.0
	test-peer address 192.168.2.1 mac 00:01:02:03:04:05 source 192.168.2.23

# PPPOE network configuration
iface pppoe inet ppp
	test pppoe
	# It could also be:
	#test-pppoe please

# I'd really appreciate a 'disabled' method for iface (#275326)
iface interface inet manual
    test missing-cable
    pre-up echo No link present.
    pre-up false

# guessnet default
iface none inet dhcp

# Example configuration for a wireless card, named eth1
# Here we're using the new autofilter option to identify "valid" profiles
# depending on their name.
auto eth1

mapping eth1
	script /usr/sbin/guessnet-ifupdown
	# We don't want to automatically connect to every open network.
	# To change this behaviour, just comment the line below or issue
	# ifup eth1=eth1-auto manually
	map !eth1-auto
	map autofilter: true
	# Since this is a wireless interface, we prefer not to a have a 
	# catchall stanza, so we don't specify any "default:" option.

iface eth1-home inet dhcp
	test wireless essid HomeNet
	wireless-essid HomeNet
	wireless-key s:MyVeryOwnPwd

iface eth1-work inet dhcp
	test wireless essid WorkNet
	# At work they use wpa auth for wireless, so we have to use 
	# wpa_supplicant in order to associate with wireless network
	wpa-ssid WorkNet
	# Passkey generated with wpa_passphrase
	wpa-psk ffecc9dc25716243282643026cdae436231fdd1a757beb948c1a10cf2fafa109
	# Eventually, we could use plaintext, such as
	# wpa-psk MySecretKey

# Following stanza will never be matched since we are using !eth1-auto
# directive in guessnet mapping. Useful if we want to enable it on
# permanent basis or to be called manually like ifup eth1=eth1-auto
iface eth1-auto inet dhcp
	# Matches any open network (meaning wifi nets not using encryption)
	test wireless open 
	wireless-essid any
	wireless-mode auto
