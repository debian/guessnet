#!/usr/bin/perl -w

# (C) Copyright 2001 Enrico Zini <zinie@cs.unibo.it>
# Based on laptop-netconf by Matt Kern <matt@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#
# This is a rewrite of laptop-netconf in perl using guessnet.
#
# This program should be an exact clone of laptop-netconf, using the same
# configuration files, having the same options, producing the same results.
#
# Please see the original laptop-netconf for documentation and examples.

use IPC::Open2;

my $cfgdir = '/etc/laptop-netconf';
my $cfgfile = $cfgdir.'/opts';
my $guessnet = '/usr/bin/guessnet';

# Read the config file from the given file 
sub readcfg ($)
{
	my ($fname) = @_;
	(-e $fname) or die "$fname does not exist";
	open(IN, "<$fname") or die "Can't read $fname: $!";
	my %cfg;
	while(<IN>)
	{
		# Skip empty lines and comments
		next if (/^\s*(?:#.*)?$/);
		if (/^\s*debug\s*(?:#.*)?$/)
		{
			$cfg{debug} = 1;
		} elsif (/^\s*device\s+(\w+)\s*(?:#.*)?$/) {
			$cfg{device} = $1;
		} elsif (/^\s*
				host\s+((?:\d{1,3}\.){3}\d{1,3})\s+
				probe\s+((?:\d{1,3}\.){3}\d{1,3})\s+
				hwaddress\s+((?:[0-9A-Fa-f]{2}\:){5}[0-9A-Fa-f]{2})\s+
				profile\s+(\w+)\s*(?:\#.*)?$/x) {
			$cfg{hosts}{$4} = {
				host => $1,
				probe => $2,
				hwaddr => $3,
				profile => $4
			};
		} else {
			die "Syntax error on line $. of $fname";
		}
	}
	close(IN);
	$cfg{device} = 'eth0' if not defined $cfg{device};
	die "No hosts found in config file $fname" if ! %{$cfg{hosts}};
	return \%cfg;
}

# Return the current network profile as found by guessnet, using the given
# configuration
sub guessnet ($)
{
	my ($cfg) = @_;
	my ($rd, $wr);
	my @invoc = ($guessnet, '-d', 'default');
	push(@invoc, '-v') if ($cfg->{debug});
	push(@invoc, $cfg->{device});
	print STDERR "Invoking `", join(' ', @invoc), "' with input:\n" if ($cfg->{debug});
	$pid = open2($rd, $wr, @invoc);
	for my $prof (keys %{$cfg->{hosts}})
	{
		my $h = $cfg->{hosts}{$prof};
		printf $wr "%s %s %s %s\n",
			$h->{host},
			$h->{hwaddr},
			$h->{probe},
			$h->{profile};
		printf STDERR "%s %s %s %s\n",
			$h->{host},
			$h->{hwaddr},
			$h->{probe},
			$h->{profile} if ($cfg->{debug});
	}
	close($wr);
	my $profile = <$rd>;
	chomp($profile);
	close($rd);
	waitpid($pid, 0);
	die "guessnet invocation was not successful" if $? == 256;
	die "guessnet exited with unknown status $?" if $? != 0;
	return $profile;
}

my $cfg = readcfg($cfgfile);
my $profile = guessnet($cfg);
print STDERR "Selected profile: $profile\n" if $cfg->{debug};
my $cmd = "$cfgdir/$profile";
my $arg = $cfg->{hosts}{$profile}{host};
print STDERR "Executing `$cmd $arg'\n" if $cfg->{debug};

# Exec already complains on its own
exec $cmd, $arg or exit 1;

exit 0;
