#!/bin/sh
# test-wireless-ap
# Licensed under the GNU GPL.  See /usr/share/common-licenses/GPL.
#
# History
# May 2005: Bugs fixed by Christoph Biedl and Thomas Hood
# Nov 2003: Modified by Thomas Hood and Klaus Wacker
# July 2003: Modified by Thomas Hood to support Aironet cards
# June 2003: Modified by John Fettig <jfettig@uiuc.edu>
# Jan 2003:  Derived from testssid by Andrew McMillan <awm@debian.org>
#            Written by Thomas Hood <jdthood@yahoo.co.uk>

set -o errexit   # -e
set -o noglob    # -f

MYNAME="$(basename $0)"
PATH=/sbin:/bin:/usr/bin

ESSID=""

usage() {
cat <<EOT
Usage:
    $MYNAME [--timeout=TIMEOUT] <IFACE> [<AP_MAC_ADDRESS>] [<IWCONFIG_ARG>]...
    $MYNAME --help|-h

Tests whether the wireless card can associate to an access
point using the given iwconfig argument pairs essid ESSID
key 123456 and so on.

Options:
	--help|-h   Print this help.
EOT
}

report_err() { echo "${MYNAME}: Error: $*" >&2 ; }

do_sleep() { LANG=C sleep "$@" ; }

is_ethernet_mac()
{
	[ "$1" ] && [ ! "${1##[0-9A-F][0-9A-F]:[0-9A-F][0-9A-F]:[0-9A-F][0-9A-F]:[0-9A-F][0-9A-F]:[0-9A-F][0-9A-F]:[0-9A-F][0-9A-F]}" ]
}

# Set ESSID to string following 'essid' in the array of function arguments
extract_ESSID()
{
	while [ "$1" ] ; do
		[ "$1" = "essid" ] && { ESSID="$2" ; return 0 ; }
		shift
	done
}

if [ "$1" = "--help" ] || [ "$1" = "-h" ]; then
    usage
	exit 0
fi

TIMEOUT=3
case "$1" in
	--timeout) TIMEOUT="$2" ; shift 2 ;;
--timeout=*) TIMEOUT="${1#--timeout=}" ; shift ;;
esac

IFACE="$1"
[ "$IFACE" ] || { report_err "Interface not specified.  Exiting." ; exit 1; }
shift

MAC_ADDRESS=$(echo "$1" | tr a-f A-F)

if is_ethernet_mac "$MAC_ADDRESS" ; then
	shift
else
	MAC_ADDRESS=""
fi

extract_ESSID "$@"

[ "$1" ] && iwconfig "$IFACE" "$@"
ifconfig "$IFACE" up

TIMELEFT="$TIMEOUT"
while test "$TIMELEFT" -gt 0 ; do
	FAILED=0
	if [ "$ESSID" ] ; then
		# Check that interface ESSID is what we are looking for
		ACTUAL_ESSID="$(iwgetid $IFACE)"
		ACTUAL_ESSID="${ACTUAL_ESSID#*ESSID:}"
		ACTUAL_ESSID="${ACTUAL_ESSID# }"
		ACTUAL_ESSID="${ACTUAL_ESSID#\"}"
		ACTUAL_ESSID="${ACTUAL_ESSID%\"}"
		[ "$ACTUAL_ESSID" = "$ESSID" ] || FAILED=1
	fi
	if [ "$FAILED" = 0 ] && [ "$MAC_ADDRESS" ] ; then
		# Check that access point MAC address is what we are looking for
		ACTUAL_MAC_ADDRESS="$(iwgetid $IFACE --ap --scheme)"
		case "$ACTUAL_MAC_ADDRESS" in
		  "FF:FF:FF:FF:FF:FF"|"ff:ff:ff:ff:ff:ff"|"44:44:44:44:44:44"|"00:00:00:00:00:00")
			ACTUAL_MAC_ADDRESS=""
			;;
		esac
		[ "$ACTUAL_MAC_ADDRESS" = "$MAC_ADDRESS" ] || FAILED=1
	fi
	if [ "$FAILED" = 0 ] ; then
		ifconfig "$IFACE" down
		exit 0
	fi
	do_sleep 1
	TIMELEFT=$(( $TIMELEFT - 1 ))
done
# Out of time
ifconfig "$IFACE" down
exit 1
