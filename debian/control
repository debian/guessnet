Source: guessnet
Section: net
Priority: optional
Maintainer: Andrew O. Shadura <bugzilla@tut.by>
Build-Depends: cdbs, debhelper (>= 7), dh-buildinfo, libnet1-dev (>= 1.1.1rel-2), libpcap-dev, libtut-dev, libwibble-dev (>= 0.1.16), libwibble-dev (<< 0.2), pkg-config, libiw-dev
Standards-Version: 3.9.3
Vcs-Git: https://salsa.debian.org/debian/guessnet.git
Vcs-Browser: https://salsa.debian.org/debian/guessnet

Package: guessnet
Architecture: any
Enhances: ifupdown
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: pppoe, ifplugd
Description: Guess which LAN a network device is connected to
 Guessnet is a non-aggressive network detection tool to use when moving
 a machine among networks which don't necessarily provide DHCP.
 .
 Guessnet takes in input a list of candidate network profiles,
 each of which includes a test description; then it runs all
 the tests in parallel and prints the name of the profile whose
 test was the first one to succeed.
 .
 Available tests are:
  * ARP probes to check for known hosts in the network
  * link beat check, to check if the interface is connected to anything
  * PPPOE check to see if there is a concentrator accessible via PPPOE
  * Checks provided by custom arbitrary scripts.
 .
 Guessnet can be used in either native mode or "ifupdown mode".
 In the latter case guessnet integrates nicely with ifupdown
 as a "mapping script".
