.\"                                      Hey, EMACS: -*- nroff -*-
.\" First parameter, NAME, should be all caps
.\" Second parameter, SECTION, should be 1-8, maybe w/ subsection
.\" other parameters are allowed: see man(7), man(1)
.TH GUESSNET 8 "4 November 2007"
.\" Please adjust this date whenever revising the manpage.
.\"
.\" Some roff macros, for reference:
.\" .nh        disable hyphenation
.\" .hy        enable hyphenation
.\" .ad l      left justify
.\" .ad b      justify to both left and right margins
.\" .nf        disable filling
.\" .fi        enable filling
.\" .br        insert line break
.\" .sp <n>    insert n+1 empty lines
.\" for manpage-specific macros, see man(7)
.SH NAME
guessnet \- guess which LAN a network interface is connected to
.SH SYNOPSIS
.B guessnet
.RI [ options ]
.RI [ network_interface ]
.br
.SH DESCRIPTION
\fBGuessnet\fP guesses which LAN a network interface is connected to.
Given a list of candidate profiles
each of which includes a test description,
\fBguessnet\fP runs all the tests in parallel
and prints the name of the profile
whose test was the first one to succeed.
If no test succeeds within a certain timeout period
then a default profile name is printed.
After printing a profile name, \fBguessnet\fP immediately
kills any tests that are still running and exits.
.P
Candidate profiles are read
either from a test description file
or, in ifupdown mode, from /etc/network/interfaces.

.SH OPTIONS
Options follow the usual GNU conventions.
In ifupdown mode, options can also be specified on the standard input
in the form "<long\-option\-name>: <value>".
.TP
.BR \-C ", " \-\-config\-file =\fIfilename\fP
Name of the configuration file to use if not specified on command line.
Default: standard input or \fI/etc/network/interfaces\fP in ifupdown mode.
.TP
.BR \-\-autofilter
Only useful when operating in ifupdown mode (see below). Instructs guessnet
to only consider logical interface names that start with physical interface
name being mapped. (ie: eth0\-home only matches when mapping eth0)
Default: \fIfalse\fP.
.TP
.B \-\-debug
Print debugging messages.
.TP
.BR \-d ", " \-\-default =\fIstring\fP
Interface name to print if no known networks are found.
Default: \fInone\fP.
.TP
.B \-\-help
Show a brief summary of command line options.
.TP
.BR \-i ", " \-\-ifupdown\-mode
Operate in ifupdown mode:
parse the input as if it is in the format of /etc/network/interfaces
and read from /etc/network/interfaces instead of the standard input
if the configuration \fIfilename\fP is not specified.
See the ifupdown mode section below for details.
.TP
.BR \-\-init\-time =\fIint\fP
Time in seconds to wait for the interface to initialize when it is not found
already up at program startup.
Default: 3 seconds.
.TP
.BR \-\-init\-delay =\fIint\fP
Sleep a given number of seconds before starting operations. May be useful in
case interface driver needs a little time to settle before reacting to 
commands.
Default: 0 seconds.
.TP
.BR \-\-iwscan\-tries =\fIint\fP
Retry wireless network scanning a given amount. Useful if your driver
needs some attempts to return a network list.
Default: 1.
.TP
.BR \-\-syslog
Send messages to syslog facility DAEMON, in addition to stderr.
.TP
.BR \-t ", " \-\-timeout =\fIint\fP
Timeout in seconds used to wait for tests to terminate.
Default: 5 seconds.
.TP
.BR \-v ", " \-\-verbose
Operate verbosely.
.TP
.B \-\-version
Show the version number of the program.

.SH "TEST DESCRIPTION FILE"
.br
\fBguessnet\fP takes as input a description of the tests it should perform.
The test description file looks like this:

.nf
  # Empty lines and lines starting with '#' are ignored.
  # Other lines contain:
  #   <profile\-name> <test\-method> <parameters>

  # At home, look for a host with the given IP and MAC address
  home peer 192.168.1.1 00:01:02:03:04:05

  # At the university, check for the presence of at least one
  # of the following hosts
  university peer 130.136.1.1 05:06:03:02:01:0A
  university peer 130.136.1.2 15:13:B3:A2:2F:CD

  # If the peer doesn't reply to ARP packets coming from 0.0.0.0
  # then you can additionally specify a source address to use
  university peer 130.136.1.2 15:13:B3:A2:2F:CD 130.136.1.250

  # For the work network use a custom script
  work command /usr/local/bin/check_work

  # Commands are executed by "sh \-c" so shell syntax can be used
  john\-irda command grep \-q `cat ~enrico/john\-irda\-id` /proc/net/irda/discovery

  # Location name and interface name are exported in NAME and IFACE
  weirdnet command /usr/local/bin/weirddetect "$NAME" "$IFACE"

  # Profile "none" is selected if no network signal is detected
  # (i.e. there is no cable plugged into the socket)
  no\-net missing\-cable

  # Match a wireless network with the given essid
  home wireless essid Home
  # You can also match the mac address of the access point
  home wireless mac 01:02:03:04:0A:0B
  # Or both
  home wireless essid Home mac 01:02:03:04:0A:0B
  # You can also match any open network
  anyopen wireless open
.fi

Every non\-comment line represents a test to perform.
.P
The first word in the line is the name that will be printed
if the test succeeds.
.P
The second word is the test type.
.P
The remainder of the line contains parameters for the selected test;
these vary depending on the test type.

.SH "IFUPDOWN MODE"

\fBifupdown\fP, Debian's standard network configuration system,
permits one to define different "logical interfaces"
(\fBifupdown\fP's name for configuration profiles)
and to choose among them when one configures
a network interface.
The choice can be delegated to an external "mapping" program.
\fBguessnet\fP can be used as such a program
if it is run in "ifupdown mode".
\fBguessnet\fP runs in ifupdown mode
if it is invoked as \fBguessnet\-ifupdown\fP
or if it is given the \fB\-\-ifupdown\-mode\fP option.
.P
In ifupdown mode
\fBguessnet\fP reads test data directly from the logical
interface definitions in /etc/network/interfaces
rather than from a separate test description file.
.P
In ifupdown mode if names are passed to \fBguessnet\fP on its
standard input then \fBguessnet\fP considers only those logical
interface definitions; otherwise it considers them all. You can have
\fBifupdown\fP deliver data to \fBguessnet\fP's standard input using the
\fImap\fP directive. See interfaces(5) for more information. If names
are preceded with "!" character then match is inverted, meaning that
all logical interfaces will be processed except for the ones specified
in standard input. You cannot mix normal and negated interface names
in the same mapping directive. Note: when using autofilter option (see
above) you can broaden or tighten the automatic matching by specifying
interface names as descripted.
.P
Please note that you have to specify the fully qualified path to \fBguessnet\fP
(/usr/sbin/guessnet\-ifupdown), as otherwise it won't be run at system boot,
as /usr/sbin is not on PATH of networking init script any more. Also, you need to
ensure /usr is actually mounted at that moment.
.P
In ifupdown mode options are selected by passing "<long\-option\-name>: <value>"
on \fBguessnet\fP's standard input.
This feature is provided because \fBifupdown\fP cannot pass command line
arguments to mapping scripts.
.P
If you prefer you can precede the \fBtest\fP keyword
in /etc/network/interfaces
with the word \fBguessnet\fP.
.P
\fBifupdown\fP does not allow two option lines in /etc/network/interfaces
to start with the same word.
To work around this limitation,
multiple \fBtest\fP (or \fBguessnet\fP) lines
can have different numerals suffixed to their initial keywords
(\fBtest1\fP, \fBtest2\fP, or \fBguessnet1\fP, \fBguessnet2\fP, and so on).
.P
Here's an example of an /etc/network/interfaces file
that has been set up for \fBguessnet\fP:

.nf
auto lo eth0

iface lo inet loopback

mapping eth0
	script /usr/sbin/guessnet\-ifupdown
	# Scan all logical interfaces
	# More options can be given here, such as:
	# map timeout: 10
	# map verbose: true
	# map debug: true
	# map iwscan-tries: 23
	map default: none

mapping eth1
	script /usr/sbin/guessnet\-ifupdown
	# Disable open net checking, just comment out if you are
	# desperate enough :) (see relative stanza below)
	map !eth1\-anyopen
	# Scan only logical interfaces named eth1\-*
	map autofilter: true

iface home inet static
	address 192.168.1.2
	netmask 255.255.255.0
	broadcast 192.168.1.255
	gateway 192.168.1.1

	# Lines for resolvconf (if you use it: see apt\-cache show resolvconf)
	# dns\-search casa
	# dns\-nameservers 192.168.1.1 192.168.2.1

	# Two tests, in case one of the two machines is down when we test
	test1 peer address 192.168.1.1 mac 00:01:02:03:04:05
	test2 peer address 192.168.1.3 mac 00:01:02:03:04:06

iface work inet static
	address 10.1.1.42
	netmask 255.255.255.0
	broadcast 10.1.1.255
	gateway 10.1.1.1
	test command /usr/local/bin/check_work

iface work2 inet static
	address 192.168.2.23
	netmask 255.255.255.0
	broadcast 192.168.2.255
	gateway 192.168.2.1
	# A source address has to be specified in case the peer
	# doesn't reply to ARP packets coming from 0.0.0.0
	test peer address 192.168.2.1 mac 00:01:02:03:04:05 source 192.168.2.23

iface eth1\-home inet static
	wireless\-essid Home
	wireless\-key s:myverysecret
	address 192.168.1.5
	netmask 255.255.255.0
	gateway 192.168.1.1
	dns\-nameservers 192.168.1.1
	# Match a wireless network with the given essid
	test wireless essid Home
	# You can also match the mac address of the access point
	#test wireless mac 01:02:03:04:0A:0B
	# Or both
	#test wireless essid Home mac 01:02:03:04:0A:0B

iface eth1\-work inet dhcp
	wireless\-essid Work
	wireless\-key s:myverysecretkey
	# Match a wireless network with the given essid
	# If you have spaces in the essid, use double quotes
	test wireless essid "Work place"

iface eth1\-anyopen inet dhcp
	# You can also match any open network, if you are desperate :)
	wireless\-essid any
	wireless\-mode auto
	test wireless open

# If nothing else is found, try DHCP
iface none inet dhcp
.fi

.SH "Supported tests"

.SS peer
.TP
.B Test description file syntax:
\fIprofile\fP \fBpeer\fP \fIIP\-address\fP [\fIMAC\-address\fP] [\fIIP\-address\fP]
.TP
.B Ifupdown mode syntax:
\fBtest peer\fP \fBaddress\fP \fIIP\-address\fP [\fBmac\fP \fIMAC\-address\fP] [\fBsource\fP \fIIP\-address\fP]
.TP
.B Description:
Look for peer using ARP.  The test will succeed if a network interface with the
specified IP address (and MAC address if specified) is connected to the local
network.
.sp
One can omit the MAC address, in which case \fBguessnet\fP only tests for the
presence of a host with the specified IP address.
.sp
If the peer whose presence you want to test for refuses to reply to ARP packets
coming from 0.0.0.0 then specify some source IP address from which the peer
will accept requests.
.sp
Multiple peers can be specified (on multiple lines) but each peer must have a
different IP address.  This restriction may be eliminated in the future.
.sp
You can also omit the IP address and only use the MAC: that is useful to test
for the existance of physical interfaces with changing IP addresses.  This kind
of scan uses an ICMP ping packet requires a source address in most cases, as
hosts tend not to reply to pings coming from nowhere.
.SS wireless
.TP
.B Test description file syntax:
\fIprofile\fP \fBwireless\fP [\fBessid\fP \fIessid\fP] [\fBmac\fP \fIMAC\-address\fP] [\fBopen\fP|\fBclosed\fP]
.TP
.B Ifupdown mode syntax:
\fBtest wireless\fP [\fBessid\fP \fIessid\fP] [\fBmac\fP \fIMAC\-address\fP] [\fBopen\fP|\fBclosed\fP]
.TP
.B Description:
Perform a wireless scan like \fBiwlist scan\fP does, and match the results.
.sp
The test succeeds if the scan reports at least one network for which all the
tests (essid, mac of the access point, network is open or closed) match.
.sp
In case more than one profile matches a network, only the first one, as found
in the configuration file, will succeed.  This allows prioritising profiles:
for example, you can prefer your home access point to an open network by
listing it first in the configuration file.
.SS missing\-cable
.TP
.B Test description file syntax:
\fIprofile\fP \fBmissing\-cable\fP
.TP
.B Ifupdown mode syntax:
\fBtest missing\-cable\fP
.TP
.B Description:
Check for link beat.
The test is successful if link beat is \fInot\fP detected.
.sp
This feature allows guessnet to detect the case
where there is no cable plugged into a network socket;
in this case it makes no sense to go through other detection phases.
.sp
This test can be used in ifupdown mode too if
a dummy logical interface is defined
that includes the \fBtest missing\-cable\fP option.
Bear in mind that when the cable is unplugged,
ifupdown will consider the interface to be configured
as this dummy logical interface.
That is somewhat counterintuitive;
one might prefer the interface to be deconfigured in that case.
Unfortunately, guessnet is not currently able to tell ifup
to refrain from configuring an interface.
The problem can be solved, however, by means of the
.BR ifplugd (8)
program.
.sp
Link beat detection is not supported on all network hardware.
If the interface or its driver does not support link beat detection
then this test does not succeed.

.SS command
.TP
.B Test description file syntax:
\fIprofile\fP \fBcommand\fP \fIcommand\fP
.TP
.B Ifupdown mode syntax:
\fBtest command\fP \fIcommand\fP
.TP
.B Description:
Test using an arbitrary command.
The test is considered successful
if the command terminates with exit status 0.
.sp
Location name and interface name are exported to the script
via the NAME and IFACE environment variables.
.sp
For backward compatibility,
\fBscript\fP can be used instead of \fBcommand\fP.

.SH "Experimental tests"

.SS pppoe
.TP
.B Test description file syntax:
\fIprofile\fP \fBpppoe\fP
.TP
.B Ifupdown mode syntax:
\fBtest pppoe\fP
.TP
.B Description:
Use the \fBpppoe\fP program to send PADI packets
in order to look for access concentrators.
The test should succeed if a PPPOE modem is present on the given interface.
.sp
Using this test requires that pppoe be installed on the system.

.SS wireless
.TP
.B Test description file syntax:
\fIprofile\fP \fBwireless\fP [\fBmac\fP \fIMAC\-address\fP] [\fBessid\fP \fIESSID\fP]
.TP
.B Ifupdown mode syntax:
\fBtest wireless\fP [\fBmac\fP \fIMAC\-address\fP] [\fBessid\fP \fIESSID\fP]
.TP
.B Description:
Test certain properties of the wireless interface.
More specifically, test the MAC address and/or
the ESSID of the associated access point.
If both are given then \fIMAC\-address\fP must precede \fIESSID\fP.
.sp
Blanks may be included in the ESSID.
For example,
.nf
    prof1 wireless essid My LAN
.fi
tests for an ESSID of "My LAN".
.sp
Note that the \fBwireless\fP test does not attempt
to change these properties; it only examines them.
This test is designed to work with programs such as
.B waproamd
which independently and dynamically manage
the wireless network adapter
to keep it associated to an access point.
.sp
Note that the \fBwireless\fP test is not yet implemented cleanly.

.P
Note that if one of several tests terminates successfully
then any other tests still running will be terminated with the KILL signal.
Therefore, test programs should not need to do any special cleanup on exit.

.SH NOTES
.SS "Getting remote host MAC addresses"
When you prepare the test data for \fBguessnet\fP you may need to know the
MAC address of a remote interface in the local network.
There are various ways to obtain this.
The easiest is to use the \fBarping\fP utility by doing "\fBarping [hostname]\fP".
If you don't have \fBarping\fP installed on your system then try the command
"\fBarp \-a [hostname]\fP" which will display the MAC address if it is in the
ARP cache of your machine.  You might want to ping the remote interface first
to make sure that you have the information in the cache.
You can also take a look at the /usr/share/doc/guessnet/examples/getmac script.
.SS "Multiple tests"
Currently \fBguessnet\fP only supports specifying one kind of test per profile.

.SH SEE ALSO
.BR ifup (8),
.BR interfaces (5),
.BR arping (8),
.BR sh (1),
.BR pppoe (8),
.BR ifplugd (8).

.SH AUTHOR
\fBGuessnet\fP was written by Enrico Zini <enrico@debian.org> with contributions from Thomas Hood. 
The ARP network detection code was taken from \fBlaptop\-netconf\fP by Matt Kern <matt@debian.org>,
which in turn in based on \fBdivine\fP by Felix von Leitner <felix@fefe.de>.
.P
The \fBGuessnet\fP webpage is at http://guessnet.alioth.debian.org .
