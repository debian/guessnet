#include "util/processrunner.h"
#include "util/output.h"

#include <stdio.h>
#include <iostream>

using namespace std;
using namespace util;

class ProcessPrinter : public ProcessListener
{
public:
	virtual void handleTermination(const std::string& tag, int status) throw ()
	{
		cout << tag << ": terminated with status " << status << endl;
	}
};


int main(int argc, char* argv[])
{
	try {
		wibble::exception::InstallUnexpected installUnexpected;

		Output::get().debug(true);

		ProcessPrinter pp;

		// Start everything
		Starter::get().start();

		fprintf(stderr, "\t\tAdding processes...\n");

		vector<string> env;
		env.push_back(string("PATH=") + SCRIPTDIR + ":/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin");

		ProcessRunner::get().addProcess("test1", "sleep 1", env, &pp);
		ProcessRunner::get().addProcess("test2", "sleep 2", env, &pp);
		ProcessRunner::get().addProcess("test2a", "sleep 2", env, &pp);
		ProcessRunner::get().addProcess("test0", "true", env, &pp);
		ProcessRunner::get().addProcess("test3", "false", env, &pp);
		ProcessRunner::get().addProcess("test2b", "sleep 2", env, &pp);
		ProcessRunner::get().addProcess("test1a", "sleep 1", env, &pp);

		Starter::get().start();

		fprintf(stderr, "\t\tSleeping...\n");
		sleep(2);
		fprintf(stderr, "\t\tShutting down...\n");
		ProcessRunner::get().shutdown();

		fprintf(stderr, "\t\tDone.\n");
	}
	catch (std::exception& e)
	{
		error("%s", e.what());
		return 1;
	}
	return 0;
}
