#include "IFace.h"
#include "util/netwatcher.h"
#include "util/output.h"

#include <stdio.h>

extern "C" {
#include <libnet.h>
}

using namespace util;

class PacketPrinter : public PacketListener
{
public:
	virtual void handleARP(struct libnet_arp_hdr* arp_header) throw ()
	{
		printf("Seen ARP\n");
	}
	virtual void handleEthernet(struct libnet_ethernet_hdr* arp_header) throw ()
	{
		printf("Seen Ethernet\n");
	}
// others to come as needed, like:
//         // virtual handleDHCP(struct libnet_hdcp_something* hdcp_header) throw () {}
};

static void printConfig(const char* tag, IFace& iface)
{
	if_params ifp = iface.getConfiguration();
	printf("%-10.10s %s: ", tag, iface.name().c_str());
	ifp.print();
}

int main(int argc, char* argv[])
{
	if (argc < 2)
		fatal_error("Usage: %s <iface>\n", argv[0]);
	try {
		wibble::exception::InstallUnexpected installUnexpected;

		NetWatcher::configure(argv[1]);

		IFace iface(argv[1]);
		if_params ifp = iface.getConfiguration();
		printConfig("Initial", iface);

		PacketPrinter pp;
		NetWatcher::get().addARPListener(&pp);
		//watcher.addEthernetListener(&pp);
		
		Starter::get().start();

		sleep(20);
		
		/*
		fprintf(stderr, "\t\tDeconfiguring interface...\n");
		system("ifconfig eth0 down");
		printConfig("Down", iface);

		{
		}

		fprintf(stderr, "\t\tConfiguring for broadcast...\n");
		iface.initBroadcast(4);
		printConfig("Broadcast", iface);

		{
			fprintf(stderr, "\t\tInitializing net watcher...\n");
			NetWatcher watcher(argv[1]);
			printConfig("Post-NS", iface);
		}

		fprintf(stderr, "\t\tRestoring interface...\n");
		iface.setConfiguration(ifp);
		printConfig("Restored", iface);
		*/
		fprintf(stderr, "\t\tDone.\n");
	}
	catch (std::exception& e)
	{
		error("%s\n", e.what());
		return 1;
	}
	return 0;
}
