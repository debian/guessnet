#include "util/output.h"
#include "IFace.h"

#include <cstdio>
#include <cstdlib>

static void printConfig(const char* tag, IFace& iface)
{
	if_params ifp = iface.getConfiguration();
	printf("%-10.10s %s: ", tag, iface.name().c_str());
	ifp.print();
}

int main(int argc, char* argv[])
{
	if (argc < 2)
		fatal_error("Usage: %s <iface>\n", argv[0]);
	try {
		fprintf(stderr, "\t\tInitializing iface object...\n");
		IFace iface(argv[1]);
		fprintf(stderr, "\t\tRetrieving configuration...\n");
		if_params ifp = iface.getConfiguration();
		fprintf(stderr, "\t\tPrinting config...\n");
		printConfig("Initial", iface);
		fprintf(stderr, "\t\tDeconfiguring interface...\n");
		system("ifconfig eth0 down");
		fprintf(stderr, "\t\tPrinting config...\n");
		printConfig("Down", iface);
		fprintf(stderr, "\t\tConfiguring for broadcast...\n");
		if_params unconf = iface.initBroadcast(4);
		printf("%-10.10s %s: ", "Pre-Bcast", iface.name().c_str());
		unconf.print();
		printConfig("Broadcast", iface);
		fprintf(stderr, "\t\tIfconfig for broadcast...\n");
		system("ifconfig");
		fprintf(stderr, "\t\tReconfiguring interface...\n");
		if_params bcast = iface.setConfiguration(ifp);
		printf("%-10.10s %s: ", "Pre-Restore", iface.name().c_str());
		bcast.print();
		printConfig("Restored", iface);
		fprintf(stderr, "\t\tDone.\n");
	}
	catch (std::exception& e)
	{
		error("%s\n", e.what());
		return 1;
	}
	return 0;
}
