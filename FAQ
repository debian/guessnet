============
Guessnet FAQ
============

-----------------
General questions
-----------------

Can you show me a simple ``/etc/network/interfaces`` file enhanced with guessnet?
---------------------------------------------------------------------------------

Sure::

  auto lo eth0
  iface lo inet loopback

  # possible guessnet default
  iface dhcp inet dhcp

  # guessnet interface to detect the absence of cable
  iface interface inet manual
  	test missing-cable
  	pre-up echo No link present.
  	pre-up false
  
  mapping eth0
  	script /usr/sbin/guessnet-ifupdown
  	
  	# List of stanzas guessnet should scan for
  	#   If none is specified, scans for all stanzas
  	#map home work
  	
  	# Profile to select when all tests fail
  	map default: dhcp
  	
  	# If no test succeed after this amount of seconds,
	# then guessnet selects the default profile.
	# Default is 5, but some network drivers need more.
  	#map timeout: 10
  	
  	# Uncomment if something goes wrong:
  	#map verbose: true
  	#map debug: true
  
  # Home network configuration
  iface home inet static
  	address 192.168.1.2
  	netmask 255.255.255.0
  	broadcast 192.168.1.255
  	gateway 192.168.1.1
  	dns-search home.loc
  	dns-nameservers 192.168.1.1
  	# Check for one of these hosts:
  	test peer address 192.168.1.1 mac 00:01:02:03:04:05
  

What is the difference between guessnet and whereami, intuitively, laptop-net, divine, ...
------------------------------------------------------------------------------------------

The main difference is that guessnet only cares about selecting a network
profile, and has no functionalities to reconfigure the system.

The main purpose of guessnet is integrating with ifupdown_, adding
automatic network detection to a Debian system with very minimum changes.


How do I get the MAC address of a remote machine?
-------------------------------------------------

Just use arping: ``arping [machine name]``.  arping can be found in debian as
the package ``iputils-arping`` or ``arping``.

Alternatively, if you don't have arping availbale, you can generate some
traffic with the remote host (such as by pinging it) and then find its MAC
address in the ARP cache using: ``/usr/sbin/arp -v`` which also works as a
normal user.


How can I tell a test to only run on some interfaces?
-----------------------------------------------------

You can do this by limiting the candidate profiles for an interface in the
mapping stanza.

The following example runs the ``no-cable`` test only on the ethernet
interface::

  mapping eth0
        script /usr/sbin/guessnet-ifupdown
        map default: auto
        map no-cable home work
  
  mapping wlan0
        script /usr/sbin/guessnet-ifupdown
        map default: auto
        map home work hotel

Or you can use the automap feature, which limits candidate profiles for 
an interface depending on their name. So for example if the interface is
named wlan0, only profiles which name starts with "wlan0-" are considered
for that interface. You can broaden or restrict this selection with usual
map syntax.

---------------
Troubleshooting
---------------

How do I know what profile has been chosen by guessnet?
-------------------------------------------------------

You have three options:

 1. see the current ifupdown mapping: ``cat /etc/network/ifstate``
 2. activate debug output by adding ``map debug: true`` to the ``mapping``
    section of ``/etc/network/interfaces``
 3. run a guessnet scan directly: ``cat /dev/null | guessnet -i eth0``.


guessnet works when eth* is up, but always returns the default profile when eth* is down, why?
----------------------------------------------------------------------------------------------

One common cause for this is a network driver which takes some time to bring up
the interface, so guessnet works fine when invoked on the commandline with the
interface already up, but when run in ifupdown to bring up the interface,
guessnet times out before the driver has finished with the initialization.

If that is the case, just increase the timeout: add ``map timeout: 10`` in the
``mapping`` stanza of ``/etc/network/interfaces`` and see if it gets better.
If it does, try decreasing the timeout to get the optimal value for you.


Link beat does not work: when guessnet tries to check it, the interface is still down for configuration
-------------------------------------------------------------------------------------------------------

This problem is caused by some network drivers going down and needing lots of
time to go up again after being configured.

Try working around the problem by adding::

  map init-time: 5

(or higher numbers) to the mapping stanza of your interfaces file: that asks
guessnet to wait for 5 seconds (instead of the default 3 seconds) after
bringing up the interface.

If it doesn't work with 5 seconds, try with 10 :)  At some point, it should
work, then you try smaller numbers to fit your needs.


I can't find a host that answers ARP requests coming from 0.0.0.0: can I use a peer scan anyway?
------------------------------------------------------------------------------------------------

Yes, using the source address.  If you had this::

  iface work inet static
     address 192.168.1.41
     test peer address 192.168.1.1 mac 00:0C:CE:03:0F:E0

try this::

  iface work inet static
     address 192.168.1.41
     test peer address 192.168.1.1 mac 00:0C:CE:03:0F:E0 source 192.168.1.42

If instead you don't know the IP address you're going to get, try using the
address of the network as a source address::

  iface work inet dhcp
     test peer address 192.168.1.1 mac 00:0C:CE:03:0F:E0 source 192.168.1.0

If using a network address (that is, ending with 0) as a source address
doesn't work either, using a valid IP (possibly one you know it's unused) might
work.


pppoe scans do not work
-----------------------

Unfortunately I implemented the PPPOE scans some time ago when I had a PPPOE
modem, but after that I never needed that feature myself, and now I don't even
have a PPPOE modem anymore.

This means that I'm in need of someone to maintain the PPPOE scans; I'm sorry I
can't help much.

However, going through old mail I just found that someone had problems with
pppoe scans and solved using a stanza like this::

  iface home inet ppp
     test pppoe
     provider providername
     up ifconfig eth0 up
     down ifconfig eth0 down

If those 'up' and 'down' commands work for you, please let me know.

If you also happen to know *why* they work, I'd be even more interested :)


I ran guessnet but the interface is still down
----------------------------------------------

You may be having a misunderstanding about the role of guessnet.

The role of guessnet is to help ifupdown_ to understand which, among various
available configurations, should be used for an interface, not to bring up the
interface.  The job of bringing up the interface belongs to ifupdown_.

It goes like this::

 # ifup eth0
 ifupdown asks guessnet: "which profile should I use for eth0?"
 ...guessnet scans...
 guessnet replies to ifupdown: "use profile Foobar"
 ifupdown brings eth0 up using profile Foobar

If you want an automatic system that brings up the network as you plug it, then
have a look at ifplugd_: it will invoke ifup and ifdown when you plug or unplug
the cable.  ifupdown_+guessnet+ifplugd_ make a nice automatic system for
transparent reconfiguration of the network.


.. _ifupdown: http://packages.debian.org/ifupdown
.. _ifplugd: http://packages.debian.org/ifplugd

..
  vim:set syntax=rst:
